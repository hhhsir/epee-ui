
import { ComponentInternalInstance, computed, ComputedRef, Ref, ref, SetupContext, unref } from 'vue'



export function useInteractive(
    instance: ComponentInternalInstance | null,
    input: Ref<HTMLInputElement | null>,
    state: any,
    nativeInputValue: ComputedRef<string>,
    { emit }: any
) {

    /**
     * 获取原生dom
     */
    const getInput = () => unref(input)

    // 设置原生value
    const setNativeInputValue = () => {
        //
        const input = getInput();
        if (!input) return;
        if (input.value === unref(nativeInputValue)) return;
        input.value = unref(nativeInputValue);
    }

    // input事件
    const handleInput = (event: Event): void => {
        const { isComposing } = state;
        if (unref(isComposing)) return;

        const input = event.target as HTMLInputElement;
        const { value } = input;
        if (value === unref(nativeInputValue)) return;
        emit('update:modelValue', value)
        emit('input', value)
    }

    const handleFocus = () => {
        const input = getInput();
        input?.focus();
    }

    const handleClear = () => {
        emit('update:modelValue', '')
        emit('input', '')
    }

    return {
        setNativeInputValue,
        handleInput,
        handleFocus,
        handleClear
    }
}

export function useInput({ modelValue }: any, instance: any) {

    const input = ref<HTMLInputElement | null>(null);

    const nativeInputValue = computed(() => {

        return unref(modelValue === null) ||
            unref(modelValue) === undefined
            ? ''
            : String(unref(modelValue));
    })

    return {
        input,
        nativeInputValue
    }

}
