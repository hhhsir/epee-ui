import './input.scss';

import { computed, getCurrentInstance, onMounted, PropType, reactive, toRefs } from 'vue';

import { useExpose } from '../../use/useExpose';
import { createNamespace } from '../../utils/createNamespace';
import { useInput, useInteractive } from './use';

const [createComponent,bem] = createNamespace("input");

export type InputSize = "large" | "middle" | "small";

export default createComponent({
  props: {
    modelValue: {
      type: (String || Number) as PropType<string | number>,
    },
    size:{
      type:String as PropType<InputSize>,
      default:'middle'
    },
    disabled: {
      type: Boolean as PropType<boolean>,
    },
  },
  emits: ["input", "update:modelValue"],
  setup(props, setupContext) {
   
    const state = reactive({
      isComposing: false,
    });

    const instance = getCurrentInstance();

    const { input, nativeInputValue } = useInput(toRefs(props), instance);

    const {
      setNativeInputValue,
      handleInput,
      handleFocus,
      handleClear,
    } = useInteractive(
      instance,
      input,
      toRefs(state),
      nativeInputValue,
      setupContext
    );

    // 对外暴露的方法
    useExpose({
        handleFocus,
        handleClear
    });



    onMounted(() => {
      setNativeInputValue();
    });

    return () => {
      const {size} = props;
      const classes = bem([
        size
      ]);

      return ( <div class={classes}>
            <input
              ref={input}
              value={props.modelValue}
              class="epee-input-inner"
              type="text"
              onInput={handleInput}
            />
          </div>
      )
    }
  },
});
