import './button.scss';

import { ButtonHTMLAttributes, computed, PropType } from 'vue';

import { createNamespace } from '../../utils/createNamespace';

const [createComponent, bem] = createNamespace("button");

export type ButtonSize = "large" | "middle" | "small";
export type ButtonShape = "circle" | "round";
export type ButtonHtmlType = "submit" | "reset" | "button" | undefined;
export default createComponent({
  props: {
    type: {
      type: String,
      default: "",
    },
    text: {
      type: String,
      default: "",
    },

    block: {
      type: Boolean,
      default: false,
    },
    loading: {
      type: Boolean,
      default: false,
    },
    disabled: {
      type: Boolean,
      default: false,
    },
    htmlType: {
      type: String as PropType<ButtonHtmlType>,
      default: "button",
    },
    size: {
      type: String as PropType<ButtonSize>,
      default: "middle",
    },
    shape: {
      type: String as PropType<ButtonShape>,
    },
  },
   emits: ["click"],
  setup(props, setupContext) {
    
    const renderText = () => {
      let text;
      if (props.loading) {
        text = "加载中...";
      } else {
        text = setupContext.slots.default
          ? setupContext.slots.default()
          : props.text;
      }
      if (text) {
        return <span>{text}</span>;
      }
    };

    const methods = {
      handleClick: (e: MouseEvent) => {
        if(!props.loading && !props.disabled){
          setupContext.emit("click", e);
        }
      },
    };
    return () => {
      const { type, shape, size, block, htmlType, loading ,disabled} = props;

      const classes = [bem([type, shape, size, { block, loading ,disabled}])];
   
      return (
        <button type={htmlType} class={classes} onClick={methods.handleClick}>
          {renderText()}
        </button>
      );
    };
  },
});
