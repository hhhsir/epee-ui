import { EmitsOptions, Ref, ref, SetupContext, unref } from "vue";



export function useTable() {
  const tableBody = ref<HTMLElement>();
  const tableHeader = ref<HTMLElement>();

  return {
    tableBody,
    tableHeader,
  };
}

export function useTableHeader(header: Ref<HTMLElement | undefined>, emit) {
  const getHeader = () => unref(header);

  const handleScroll = (e: Event) => {
    const target = e.target as HTMLElement;
    emit("scrollLeft", target.scrollLeft as number);
  };

  const scrollToLeft = (left: number) => {
    getHeader().scrollLeft = left;
  };
  return {
    handleScroll,
    scrollToLeft,
  };
}

export function useTableBody(body: Ref<HTMLElement | undefined>, emit) {
  const getBody = () => unref(body);

  const handleScroll = (e: Event) => {
    const target = e.target as HTMLElement;
    emit("scrollLeft", target.scrollLeft as number);
  };

  const scrollToLeft = (left: number) => {
    getBody().scrollLeft = left;
  };
  return {
    handleScroll,
    scrollToLeft,
  };
}
