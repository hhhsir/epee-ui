import "./table-header.scss";
import "./table.scss";

import { useExpose } from "../../use/useExpose";
import {useTable, useTableHeader} from './use'

import { createNamespace } from "../../utils/createNamespace";
import { useEventListener } from "../../use/useEventListener";

const [createComponent, bem] = createNamespace("table-header");

export interface TableHeaderRef {
  scrollToLeft
}

export default createComponent({
  emits:['scrollLeft'],
  setup(props, {emit}) {
    const { tableHeader } = useTable();
    const { handleScroll ,scrollToLeft} = useTableHeader(tableHeader ,emit);
    useExpose({
      scrollToLeft
    });

    return () => {
      const classes = [bem()];

      return (
        <div class="epee-table-header-wrap" onScroll={handleScroll}   ref={tableHeader}>
          <div class={classes} style={{ width: "800px" }}>
            table-headertable-headertable-headertable-headertable-header
          </div>
        </div>
      );
    };
  },
});
