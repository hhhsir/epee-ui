import "./table-body.scss";

import { createNamespace } from "../../utils/createNamespace";

const [createComponent, bem] = createNamespace("table-body");

import debounce from "lodash.debounce";

import "./table.scss";


import { useExpose } from "../../use/useExpose";
import { useTable, useTableBody } from './use';
import { DefineComponent } from 'vue';


export interface TableBodyRef extends DefineComponent{
  scrollToLeft
}

export default createComponent({
  setup(props, {emit}) {
    const {tableBody} = useTable();
    const {handleScroll,scrollToLeft} = useTableBody(tableBody,emit);

    useExpose({
      scrollToLeft
    });
    return () => {
      const classes = [bem()];

     
      return (
        <div class="epee-table-body-wrap" ref={tableBody}  onScroll={handleScroll}>
          <div class={classes} style={{ width: "800px" }}>
            <h1>
              table-bodytable-bodytable-bodytable-bodytable-bodytable-bodytable-bodytable-body
            </h1>
            <h1>
              table-bodytable-bodytable-bodytable-bodytable-bodytable-bodytable-bodytable-body
            </h1>
            <h1>
              table-bodytable-bodytable-bodytable-bodytable-bodytable-bodytable-bodytable-body
            </h1>
            <h1>
              table-bodytable-bodytable-bodytable-bodytable-bodytable-bodytable-bodytable-body
            </h1>
            <h1>
              table-bodytable-bodytable-bodytable-bodytable-bodytable-bodytable-bodytable-body
            </h1>
            <h1>
              table-bodytable-bodytable-bodytable-bodytable-bodytable-bodytable-bodytable-body
            </h1>
            <h1>
              table-bodytable-bodytable-bodytable-bodytable-bodytable-bodytable-bodytable-body
            </h1>
          </div>
        </div>
      );
    };
  },
});
