import "./table.scss";

import { createNamespace } from "../../utils/createNamespace";
import TableBody ,{TableBodyRef} from "./table-body";
import TableHeader,{TableHeaderRef} from "./table-header";

import { defineComponent, getCurrentInstance, onMounted, Ref, ref } from "vue";

const [createComponent, bem] = createNamespace("table");


/**
 * table父组件,状态在这里管理
 */

export default createComponent({
  setup() {
    const tableBody = ref<TableBodyRef>();
    const tableHeader = ref<TableHeaderRef>();
    const classes = [bem()];
    const onScrollLeft = (left: number) => {
      tableHeader.value.scrollToLeft(left);
      tableBody.value.scrollToLeft(left);
    };
  

    return () => {
      return (
        <div class={classes}>
          <TableHeader {...{ onScrollLeft: onScrollLeft }} ref={tableHeader} />
          <TableBody {...{ onScrollLeft: onScrollLeft }} ref={tableBody} />
        </div>
      );
    };
  },
});
