import { createBEM } from './bem';
import { createComponent } from './createComponent';

export function createNamespace(name: string) {
  name = "epee-" + name;
  return [createComponent(name),createBEM(name)] as const;
}
