
import { isArray, isString } from '../utils/shared';

export type Mod = string | { [key: string]: any };
export type Mods = Mod | Mod[];     // 字符串,数组,对象


function gen(name: string, mods?: Mods): string {
    if (!mods) {
        return ''
    }
    // 字符串
    if (isString(mods)) {
        return ` ${name}-${mods}`
    }
    // 数组  递归到字符串那里
    if (isArray(mods)) {
        return mods.reduce<string>(
            (ret, item) => ret + gen(name, item),
            '')
    }
    // 对象 递归到字符串那里
    return Object.keys(mods).reduce(
        (ret, key) => ret + (mods[key] ? gen(name, key) : ''),
        ''
    )
}

/**
 * bem规范文档  https://bemcss.com/
 * epee-button
 * @param name epee-button
 */

export function createBEM(name: string) {

    return function (el?: Mods, mods?: Mods): Mods {
        if (el && !isString(el)) {
            mods = el;
            el = ''
        }
        el = el ? `${name}__${el}` : name;
        // gen(el, mods)   epee-button--primary epee-button--middle
        return `${el}${gen(el, mods)}`

    }
}

export type BEM = ReturnType<typeof createBEM>;