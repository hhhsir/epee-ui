import { App, Plugin } from 'vue';

export function installPlugins<T extends { name: string }>(
    Component: T,
    plugins?: Plugin[],
) {
    return {
        ...Component,
        install(app: App) {
            // 用Component.name注册
            app.component(Component.name, Component);
            !!plugins && (plugins.forEach(app.use))
        },
    }
}