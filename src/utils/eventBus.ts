
import { getCurrentInstance } from 'vue'
import {kebabCase} from './kebabCase'

function emitName2ListenName(emitName: string): string {
    const match = emitName.match(/update([A-Z])(.*)/)
    if (match) {
        return `update:${match[1].toLowerCase()}${match[2]}`
    }
    return kebabCase(emitName)!
}

type EventListener<EmitsValue> = EmitsValue extends (...args: any[]) => any ? Parameters<EmitsValue> : never


export type ComponentEvent<Emit> = {
    emit: { [key in keyof Emit]: (...args: EventListener<Emit[key]>) => void },
    on: { [key in keyof Emit]: (cb: (...args: EventListener<Emit[key]>) => void) => void },
    once: { [key in keyof Emit]: (cb: (...args: EventListener<Emit[key]>) => void) => void },
    off: { [key in keyof Emit]: (cb: (...args: EventListener<Emit[key]>) => void) => void },
}

export type SimpleFunction = (...args: any[]) => any
type Listener = (SimpleFunction & { fn?: any })
type ListenName = string | symbol
export type PlainEvent = ReturnType<typeof createPlainEvent>

export function createPlainEvent() {

    /**
     * 第一次调用getEvents的时候才创建map对象
     * @author  韦胜健
     * @date    2020/10/17 11:27
     */
    const getListenMap = (() => {
        let events: Map<ListenName, Listener[]>;
        return () => {
            if (!events) {
                events = new Map<ListenName, Listener[]>()
            }
            return events
        }
    })()

    const event = {
        on: (listenName: ListenName, fn: SimpleFunction) => {
            const listenMap = getListenMap()
            const map = listenMap.get(listenName)
            if (!!map) {
                map.push(fn)
            } else {
                listenMap.set(listenName, [fn])
            }
            return () => event.off(listenName, fn)
        },
        once: (listenName: ListenName, fn: SimpleFunction) => {
            const on: Listener = (...args: any[]) => {
                event.off(listenName, fn)
                fn(...args)
            }
            on.fn = fn
            event.on(listenName, on)
            return () => event.off(listenName, on)
        },
        off: (listenName: ListenName, fn?: SimpleFunction) => {
            const listenMap = getListenMap()

            const listeners = listenMap.get(listenName)
            if (!listeners) {
                return;
            }

            /*移除listenName的所有监听器*/
            if (!fn) {
                listenMap.set(listenName, [])
                return
            }

            for (let i = 0; i < listeners.length; i++) {
                const listener = listeners[i];
                if (fn === listener || (!!listener.fn && fn === listener.fn)) {
                    listeners.splice(i, 1)
                    break
                }
            }
        },
        emit: (listenName: ListenName, ...args: any[]) => {
            const listeners = getListenMap().get(listenName)
            if (!!listeners) {
                listeners.forEach(listener => listener(...args))
            }
        },
    }

    return event
}

export function getComponentEmit<T>(emitObject: T): T {
    return {
        change: null,
        ...Object.keys(emitObject || {}).reduce((ret: any, key: string) => {
            ret[emitName2ListenName(key)] = (emitObject as any)[key]
            return ret
        }, {} as any),
    }
}

export function useEvent<T>(emitObject: T): ComponentEvent<T> {

    const ctx = getCurrentInstance()!
    const event = createPlainEvent()

    const emit = {} as any;
    const on = {} as any;
    const once = {} as any;
    const off = {} as any;


    Object.keys(emitObject || {}).forEach(key => {
        /*派发事件名称，横杠命名*/
        const kebabCaseName = emitName2ListenName(key)

        emit[key] = (...args: any[]) => {
            ctx.emit(kebabCaseName, ...args)
            event.emit(kebabCaseName, ...args)
            if (key === 'updateModelValue') {
                ctx.emit('change', ...args)
                event.emit('change', ...args)
            }
        }
        on[key] = (fn: SimpleFunction) => event.on(kebabCaseName, fn)
        once[key] = (fn: SimpleFunction) => event.once(kebabCaseName, fn)
        off[key] = (fn: SimpleFunction) => event.off(kebabCaseName, fn)
    })

    return {
        emit, on, once, off
    } as any
}
