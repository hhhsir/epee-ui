import { App, ComponentOptionsWithObjectProps, defineComponent } from 'vue';

export function createComponent(name: string) {
    return function (componentOptions: ComponentOptionsWithObjectProps) {
        componentOptions.name = name;
        componentOptions.install = (app: App) => {
            app.component(name as string,componentOptions);
            // app.component()
        }
        return defineComponent(componentOptions)
    } as typeof defineComponent;

}