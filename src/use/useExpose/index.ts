import { getCurrentInstance } from 'vue';

import { extend } from '../../../src/utils/shared';

export function useExpose(apis: Record<string, any>) {
    const instance = getCurrentInstance();
    if (instance) {
        extend(instance.proxy, apis);
    }

}