/**
 * 导出声明
 * 识别.vue结尾的文件,导出的类型都是component
 * (ts只认识ts/tsx结尾的文件)
 */

declare module '*.vue' {
  import  { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}
declare module '*.tsx' {
  import  { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}
