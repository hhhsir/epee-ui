import { App } from 'vue';

import EpeeButton from './packages/button';
import EpeeInput from './packages/input';
import EpeeTable from './packages/table';

const plugins = [

    EpeeInput,
    EpeeButton,
    EpeeTable
]



function install(app: App) {
    plugins.forEach(plugin=>{
        plugin.install(app);
    })
}

export default {
    install,
}

export {

    EpeeInput,
    EpeeButton,
    EpeeTable,
    install,
}