# vue3组件库,还在开发中...

V0.1.12

## TODO

 - 文档 docs
  - 文档编写,markdown-loader
  - readme
 - 主题 theme
  - 主题样式变量
  - 主题使用方式
  - 主题切换方式
  - 主题生成
 - 组件 component
  - 服务组件
  - 基础组件
- 工具  utils
  - emitter
  - designComponent完善
- 类型  types
  - d.ts定义
- 测试  test
  - 测试项目/测试代码
- 部署
  - 自动部署/发布

### 快速开始

```s
npm i epee-ui
```

main.js/main.ts全局注册

```js
import { createApp } from 'vue'
import App from './App.vue'

import EpeeUI from 'epee-ui'
import 'epee-ui/dist/index.css'

createApp(App).use(EpeeUI).mount('#app')
```

使用

```vue
<template>

  <epee-button>button</epee-button>
 
</template>

<script>


export default {
  name: 'App',
  components: {
   
  }
}
</script>

```


### 配置按需引入组件(推荐,类型可以正常引入)

`$ npm i   babel-plugin-import  -D`

babel.config.js

```s
module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  plugins: [
    ['import', {
      libraryName: 'epee-ui',
      libraryDirectory:"src/packages",
      style: false,
      customName:(name)=>{
        
        return `epee-ui/src/packages/${name.split('-')[1]}`
      }
    }, 'epee-ui']
  ]
}


```
使用

```vue
<template>
  <div>
     <epee-button loading>loading</epee-button>
  </div>
</template>

<script lang="ts">
import { defineComponent } from "vue";
import  {EpeeButton,EpeeInput} from "epee-ui";
export default defineComponent({
  name: "App",
  components: {
    EpeeButton,
    EpeeInput
  },
 
});
</script>


```

如果修改babel.config.js后,发现配置并未生效

手动删除`node_modules/.cache`文件夹

### 手动引入所需插件(不推荐,类型丢失,但是可以正常渲染)

```s
import  EpeeButton from "epee-ui/src/packages/button";
import EpeeInput from 'epee-ui/src/packages/input'

...

  components: {
    EpeeButton,
    EpeeInput
  }

...
```

### gitee

暂时用gitee管理

> https://gitee.com/hhhsir/epee-ui

### document

> http://hhhsir.gitee.io/epee-ui

### 相关资料
- [element-ui2] (https://github.com/ElemeFE/element):  家喻户晓的element,基于vue2
- [element-ui3] (https://github.com/kkbjs/element3): kkbjs维护的支持vue3的element
- [vant] (https://github.com/youzan/vant): vant3.0支持vue3,但是好像问题比较多
- [组件库成品plain-ui](http://plain-pot.gitee.io/plain-ui-doc/#home%2Fintroduce)：这个组件库还是基于Vue2.0开发，不过使用的是 `@vue/composition-api + typescript`，所以每个组件的实现原理与 `Vue3.0`大致相同；
- [Vue3.0官方文档（英文）](https://v3.vuejs.org/guide/introduction.html)
- [Vue3.0官方文档（中文）](https://v3.cn.vuejs.org/guide/introduction.html)
- [渲染函数：官方文档](https://v3.vuejs.org/guide/render-function.html#jsx)
- [渲染函数：jsx-next github](https://github.com/vuejs/jsx-next#installation)
- [Vue3.0文档（中文）](http://martsforever-snapshot.gitee.io/vue-docs-next-zh-cn/)：我在码云上同步过来的中文文档，不用翻墙，访问快很多；
- [Composition Api](http://martsforever-snapshot.gitee.io/vue-docs-next-zh-cn/guide/composition-api-introduction.html)：在Vue3.0文档中一样可以找到，这里给出直接访问地址。
- [Typescript Deep Dive](http://martsforever-snapshot.gitee.io/typescript-book-chinese/)：我在码云上同步过来的 `Typescript Deep Dive`一书的中文文档，不用翻墙访问很快；
- [@vue/cli Vue官方脚手架](https://cli.vuejs.org/zh/)：官方推荐的用于创建Vue工程脚手架工具
- [Vite](https://www.npmjs.com/package/vite)：尤雨溪大佬新出的，旨在替代webpack-dev的开发工具；



### 开发说明

**目录结构**

```



```



#### 记录

**rollup+webpack所需要的依赖**

```bash
npm i autoprefixer@8 babel-loader@8 css-loader@5 mini-css-extract-plugin postcss@8 postcss-loader@4 rollup-plugin-postcss@3 rollup-plugin-terser sass sass-loader@8 ts-loader@8 typescript webpack@4 webpack-cli@4 webpack-merge @rollup/plugin-babel @rollup/plugin-commonjs @rollup/plugin-node-resolve @rollup/plugin-typescript -D
```


作业1    第二步   Object.defineProperty 对象每个key都要预先写好,否则不会依赖收集

作业2  第三步  formData给了一个新的对象,重新遍历了这个对象,进行依赖收集

作业3  加了新字段
作业4  全量赋值对象

作业5  对象加了新字段,没有依赖收集
作业6  .sync做了响应式初始化