这是 part 的 input 组件

> https://gitee.com/speedly_admin/zhufeng-vue3ts-assembly202010/blob/master/part02/%E6%BA%90%E7%A0%81/v3-components-wsj/src/packages/input/input.tsx

```ts
export default designComponent({
  name: "pl-input",
  props: {
    status: { type: String, default: "primary" },
  },
  setup(props) {
    const modelValue = ref("");
    const inputRef = ref(null as null | HTMLInputElement);

    const classes = computed(() => [
      "pl-input",
      `pl-input-status-${props.status}`,
    ]);

    const methods = {
      focus: (flag: boolean) => {
        inputRef.value!.focus();
        if (flag) {
          modelValue.value = "";
        }
      },
      clear: () => {
        modelValue.value = "";
      },
    };

    return {
      refer: {
        methods,
        modelValue,
      },
      render: () => (
        <div class={classes.value}>
          <input
            class="pl-input-inner"
            type="text"
            v-model={modelValue.value}
            ref={inputRef}
          />
          <button onClick={methods.clear}>clear</button>
          <button onClick={() => methods.focus(true)}>focus</button>
        </div>
      ),
    };
  },
});
```

方便理解,我删一点无关的东西,最终目的是

```ts
export default designComponent({
  name: "pl-input",
  setup(props) {
    const inputRef = ref(null as null | HTMLInputElement);
    const methods = {
      focus: () => {
        inputRef.value!.focus();
      },
    };
    return {
      refer: {
        methods,
      },
      render: () => (
        <div>
          <input ref={inputRef} />
          <button onClick={() => methods.focus}>focus</button>
        </div>
      ),
    };
  },
});
```

这个函数最终导出的时候是这样的(先不看 use)(方便看我函数简写去掉了)

```ts
export default defineComponent({
  name: "pl-input",
  setup(props: Props, setupContext: SetupContext) {
    const methods = {
      focus: () => {
        inputRef.value!.focus();
      },
    };
    const ctx = getCurrentInstance();

    Object.assign(ctx.proxy, {
      methods: methods,
    });

    return () => {
      return (
        <div>
          <input ref={inputRef} />
          <button onClick={() => methods.focus}>focus</button>
        </div>
      );
    };
  },
});
```

这样做了之后会发生什么呢?

```ts
const ctx = getCurrentInstance();
Object.assign(ctx.proxy, {
  methods: methods,
});
```

写点伪代码,大概意思下 `console.log(plInput.value)`这个运行下你们就懂了,
大家可以先不用use里面的ref函数取方法,先用 `plInput.value`  ,然后再去看`(ctx as any).$refs[refName].$._refer`  (后面更新了这一串)

```
<pl-input ref={plInput} />

const plInput = ref();

onmounted(()=>{  console.log(plInput.value) })  

setup(){
     return {
        plInput
    }
}

```
