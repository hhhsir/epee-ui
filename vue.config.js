const path = require('path');
const webpack = require('webpack')


const resolve = filePath => path.join(__dirname, './', filePath)

module.exports = {
    outputDir: 'docs',
    publicPath: '/epee-ui/',
    devServer: {
        port: '3366'
    },
    pages: {
        index: {
            entry: resolve('story/main.ts'),
            template: 'public/index.html',
            filename: 'index.html',
            title: 'epee-ui',
        },
    },

     
    
    chainWebpack: config => {
        config.plugins
            .delete('prefetch-index')
            .delete('preload-index');
        
        config.resolve.alias
            .set('story', resolve('story'))
            .set('src', resolve('src'))
    },
    css: {
        loaderOptions: {
            sass: {
                prependData: `@import "src/style/global-import.scss";`
            },
        }
    },
   
   
}