import { App } from 'vue';

export class Component {
  static name: string;

  static install: (app: App) => any;

  $props: Record<string, any>;
}
