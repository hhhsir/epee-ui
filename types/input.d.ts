import { Component } from './component'

export class Input extends Component {
    handleFocus(): void;
    handleClear(): void;
}