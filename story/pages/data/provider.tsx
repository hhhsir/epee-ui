
import { designComponent } from 'src';

import ColorCard from './ColorCard.vue'

export default designComponent({
    name: 'ProviderMain',
    provideRefer: true,
    setup() {
     
        return {
            refer:{
                age:10
            },
            render: () => {
                return (<ColorCard />)
            }
        }
    }
})


