import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";

import App from './App.vue'
import Story from "./components/index.vue";

export function getRoutes() {
    return  [
        {
            path: '/',
            name: 'Layout',
            redirect: '/dashboard',
            component: Story,

            children: [
                {
                path: '/dashboard',
                name: 'Dashboard',
                component: () => import('./pages/dashboard.vue')
            },
            {
                path: '/normal',
                component: Story,
                name:'Normal',
                children: [
                    {
                        path: '/normal/button',
                        name: 'Button',
                        component: () => import('./pages/normal/button.vue')
                    }
                ]
            },
            {
                path: '/form',
                component: Story,
                name:'Form',
                children: [
                  
                    {
                        path: '/form/input',
                        name: 'Input',
                        component: () => import('./pages/normal/input.vue')
                    }
                ]
            },
            {
                path: '/demo',
                component: Story,
                name:'Demo',
                children: [
                  
                    {
                        path: '/demo/formScroll',
                        name: 'FormScroll',
                        component: () => import('./pages/demo/demo-use-event.vue')
                    }
                ]
            },
         
            ]

        },

    ]

}


export const router = createRouter({
    history: createWebHashHistory('epee-ui'),
    routes:getRoutes(),
    linkActiveClass: 'active'
})
