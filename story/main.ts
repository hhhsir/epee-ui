import { createApp } from 'vue'
import App from './App.vue'

import 'normalize.css/normalize.css'
import './common/style/index.scss'
import V3Component from 'src'
import {router} from  './router';



const app = createApp(App)
app.use(V3Component).use(router).mount('#app')

